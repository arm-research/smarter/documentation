# documentation

* [Installation](https://gitlab.com/arm-research/smarter/example/-/blob/master/README.md)
* [Tutorial](tutorial.md) - how to deploy your own applications
* [Troubleshooting](troubleshooting.md) - fixes for common issues
