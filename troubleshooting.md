# Troubleshooting


## Problems seen while FLASHING:
```bash
$ sudo ./smarter-doflash.sh <ARGS>
...
ERR: could not retrieve chip ID
```
Explanation: Xavier was not booted into recovery mode.

Fix: Hold middle button down, tap power, then release middle button before attempting flashing.

---

```bash
$ sudo ./smarter-doflash.sh <ARGS>
...
Error: Return value 8
Command tegrarcm_v2 --download bct_bootrom br_bct_BR.bct --download bct_mb1 mb1_bct_MB1_sigheader.bct.encrypt --download bct_mem mem_rcm_sigheader.bct.encrypt
```
Explanation: Likely trying to flash an image built for an incorrect device.

Fix: Check image target.

---

```bash
$ sudo ./smarter-doflash.sh <ARGS>
...
Error: Return value 4
Command tegradevflash_v2 --pt flash.xml.bin --create
```
Explanation: Mismatched partition sizes indicates either image for an incorrect device or backward incompatible updates.

Fix: Revert to previous working version.


## Problems seen while RUNNING:
```bash
# k3s check-config
FATA[0000] exec format error
```
Explanation: k3s failed to unpack its dependencies.

Fix: either reflash, or:
```bash
# /etc/init.d/k3s stop
# rm -rf /var/lib/rancher
# k3s kubectl
# /etc/init.d/k3s start
```

---

```bash
# tail /var/log/edge-core.log
2020-10-26 19:22:05.451 tid:   9391 [ERR ][edgecc]: Device not configured for Device Management - exit
```
Explanation: Pelion BYOC malformed or nonexistent.

Fix: either reflash, or:
```bash
$ scp byoc.cert root@device:/tmp/byoc.cert
# shutdown -r
```
