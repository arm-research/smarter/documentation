# pelion-edge-provisioning

Right now the default in the yocto builds is for BYOC mode to simplify use of a common image.  If you want to use factory mode or developer mode, you will have to edit configuration options in the meta-pelion-edge layer.  This will be simplified in the future.

## Terminology

* BYOC - bring your own certificate
* CBOR - concise binary object representation
* developer certificate - developer mode certificate for devices (replaced by factory in production)
* update resource - certificate enabling OTA upgrade on devices

## Obtaining Certificates

1. Create and download a developer certificate to allow the edge gateway to connect to your Pelion account:
    1. In the Pelion Device Management Portal, go to Device Identity > Certificates.
    2. Create a developer certificate.
    3. Download the certificate mbed_cloud_dev_credentials.c
2. Initialize the manifest tool to create the corresponding update_default_resources.c file. This enables firmware updates to be applied to the gateway device:
   1. Determine your product model identifier: TBD: how?
   2. Initialize the manifest-tool
      1. ```pip install manifest-tool```
         1. *NOTE: Ubuntu 20.04.1 makes this interesting as you have to jump through hoops for pip versus pip3*
      2. ```export PATH=~/.local/bin:$PATH```
      3. ```echo MBED_CLOUD_SDK_API_KEY="you_api_key_here" >> ~/.env```
   3. ```manifest-tool init -m "<product model identifier>" -V 42fa7b48-1a65-43aa-890f-8c704daade54 -q```

    *Note: To unlock the rich node features, such as gateway logs and the gateway terminal in the Pelion web Portal, pass the command-line parameter -V 42fa7b48-1a65-43aa-890f-8c704daade54 to the manifest tool. Contact the service continuity team at Arm to request they enable Edge gateway features in your Pelion web Portal account. By default, the features are not enabled.*

## Factory mode

* See [Pelion Provisioning Overview](https://www.pelion.com/docs/device-management/current/provisioning-process/index.html)
* mbed-fcc installed on device, but in order to use, you'll likely need to change smarter-firstrun.sh
    * /opt/arm/factory-configurator-client-armcompiled.elf

## Developer mode

* before initializing your smarter yocto build environment (with setup-environment), make sure you have set environment varibales with paths to your developer and update certificates:
```
% setenv MBED_CLOUD_IDENTITY_CERT_FILE=/path/to/mbed_cloud_dev_credentials.c
% setenv MBED_CLOUD_UPDATE_FILE=/path/to/update_default_resources.c
% . setup-environment
```

## BYOC mode (default)

* Obtain credentials following instructions above
* Obtain [edge_tool](https://github.com/ARMmbed/mbed-edge/blob/master/edge-tool/README.md)
* use edge_tool to create CBOR
  1. ```cd mbed-edge/edge-tool```
  2. ```./bootstrap-edge-tool-env```
  3. ```source ./mbed-cloud-sdk-env/bin/activate```
  4. ```./edge_tool.py convert-dev-cert --development-certificate <path> --cbor <path> --update-resource <path> ```
     1. *BUG: edge_tool.py seems to hang after execution, hard to judge success*
* Provide CBOR during flash-time as BYOC
  1. ```smarter-doflash.sh --byoc <boc-location> <other-args>```
  2. This will ensure that mbed-edge-core starts with -c <byoc file> when the node initializes
* *Alternately*: put BYOC file on target in (TBD: Path)
  * The flashing process puts the byoc file in the root of the file system (TBD: we should change this) as byoc.cert
  * Simply copying your byoc.cert to the root of the file system on the image should trigger your mbed-edge-core to use it on next restart
