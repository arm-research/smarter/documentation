# SMARTER Development Tutorial

This document is intended to provide development/operations teams instruction on how to package applications and deploy them to the edge using SMARTER's infrastructure.
It assumes that you have already brought up a working SMARTER cluster using the guides found in the [README](https://gitlab.com/arm-research/smarter/example/-/blob/master/README.md). 

In this tutorial we will reference node names of the form smarter_xavier_xxx. This assumes you have provisioned a Xavier with our custom yocto image. If you are using your own custom nodes/node-names, replace all references to smarter_xavier_xxx with your node name.

# Basics
## Lesson 1: A yaml-only app
### Deploy a pod
To demonstrate the most basic example of deploying an application to the edge lets start off with the yaml description of an app.
In kubernetes the atomic unit of deployment is the pod. For most applications deployed to the edge, we will assume that one pod encapsulates one docker container, however it is possible to deploy more than one container in a pod. In the following examples, we are using publicly available docker images, as opposed to packaging up our own custom apps.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-first-pod
spec:
  automountServiceAccountToken: false
  dnsPolicy: ClusterFirst # Pod DNS policy specifying to use cluster dns before using host dns info
  hostname: my-first-pod  # Hostname pod will be accessible from for other pods
  containers:
  - name: client
    image: alpine:3.9     # Image the pod will use
    command: ["/bin/sh"]  # Command override of above image
    args: ["-c","while :; do echo \"Hello from the edge\"; sleep 5; done"] # Arguments override passed to above command
    imagePullPolicy: IfNotPresent # Image pull policy specifying image will only be pulled if not present on node (other popular option is 'Always')
    resources:
      limits:             # If sufficient resource available, container allowed to use up this resource limit
        cpu: 200m         # https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#meaning-of-cpu
        memory: 10Mi
      requests:           # Kubelet reserves at least this amount of system resource specifically for this container to use
        cpu: 200m         # https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#meaning-of-cpu
        memory: 10Mi
  restartPolicy: Always   # If pod exits with error status, restart pod always, other options are 'Never', 'OnFailure'
```

Create a file named `my-first-pod.yaml` in your current directory, and using kubectl we can run the pod on a node in our cluster by running:
```bash
kubectl apply -f my-first-pod.yaml
```

With the pod object applied to the cluster we can then query it's status by running:
```bash
kubectl get pods -o wide
```

Which should return something like:
```bash
NAME            READY     STATUS    RESTARTS   AGE       IP               NODE                 NOMINATED NODE   READINESS GATES
my-first-pod    1/1       Running   0          2m10s     172.38.0.5       smarter_xavier_xxx   <none>           <none>
```

We can see that the pod `my-first-pod` has been successfully created and is now running in the cluster. To view it's logs, we can run:
```bash
kubectl logs my-first-pod
```

And we should see the message `Hello from the edge` repeated over and over.

Another tool to use to describe a pods status is `describe`. Run the following on the pod:
```bash
kubectl describe pod my-first-pod
```
and you will see an output showing the current pod configuration and status. The `Events` list on the pod is frequently useful to catch events which modify the state of the pod such as resource over-utilization etc. 

To delete stop the application, we simply can run either:
```bash
kubectl delete pod my-first-pod
```
or
```bash
kubectl delete -f my-first-pod.yaml
```

It is important to note that with pod description, we will only be able to deploy a single copy of our app into a single node in our cluster, when in real-world scenarios, we will generally want to deploy the app to multiple nodes. To do this we use a DaemonSet as per the next section.

### Deploy a DaemonSet
Deploying applications directly via pods is possible in k8s, however with SMARTER, we prefer to use a higher level controller for application deployment called a `DaemonSet`. DaemonSets allow you to declare you want a single application pod running on every node or a specified subset of nodes in your cluster. This maps well to the edge, where generally our workloads do not need multiple copies like they may in the cloud. To deploy an application using a DaemonSet, copy the following yaml into a file named `my-first-ds.yaml`:
```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: my-first-ds-app
  namespace: default
  labels:
    k8s-app: my-first-ds-app
spec:
  selector:
    matchLabels:
      name: my-first-ds-app
  template:
    metadata:
      labels:
        name: my-first-ds-app
    spec:
    #   nodeSelector:
    #     my-first-ds-app: enabled
      hostname: my-first-ds-app
      containers:
      - name: my-first-ds-app
        imagePullPolicy: IfNotPresent
        image: alpine:3.9
        command: ["/bin/sh"]
        args: ["-c","while :; do echo \"Hello from the edge\"; sleep 5; done"]
        resources:
          limits:
            cpu: 100m
            memory: 10Mi
          requests:
            cpu: 100m
            memory: 10Mi
      terminationGracePeriodSeconds: 30
```

We can register our DaemonSet in our cluster by running:
```bash
kubectl apply -f my-first-ds.yaml
```

After a few seconds we can view the pods in our cluster by again running:
```bash
kubectl get pods -o wide
```
We get the following output:
```bash
NAME                      READY     STATUS    RESTARTS   AGE       IP           NODE                 NOMINATED NODE   READINESS GATES
my-first-ds-app-<ID>      1/1       Running   0          8s        172.38.0.3   smarter-xavier_xxx   <none>           <none>
my-first-ds-app-<ID>      1/1       Running   0          8s        172.38.0.5   smarter-xavier_xxx   <none>           <none>
```

We can see here that we have a single copy of our simple bash app running on each the nodes in our cluster.
You may notice that in the yaml above, we have commented out the `nodeSelector` section in the spec. Because of this, the DaemonSet will attempt to run a single copy of the pod on **every** node in the cluster. In a real world scenario, we may only want to run the pod on a select few nodes in our cluster, which is where the nodeSelector comes in handy. To demonstrate, uncomment the two lines in the yaml file you just created and update your DaemonSet by reapplying the yaml file:
```bash
kubectl apply -f my-first-ds.yaml
```

After a few seconds we can view the pods in our cluster by again running:
```bash
kubectl get pods -o wide
```
We get the following output:
```bash
No resources found.
```

There are no longer two pods running in our cluster, as there are no nodes in our cluster which have the label `my-first-ds-app: enabled`. 

You can see all the labels on a node by running:
```bash
kubectl get node smarter_xavier_xxx -o wide --show-labels
```

We can look at the status of our DaemonSet by running the command:
```bash
kubectl get ds my-first-ds-app
```
Which will yield the output:
```bash
NAME                DESIRED   CURRENT   READY     UP-TO-DATE   AVAILABLE   NODE SELECTOR              AGE
my-first-ds-app     0         0         0         0            0           my-first-ds-app=enabled   7m28s
```

We can see there are no desired pods, as no nodes have the correct node selector label.
To label a node and make a pod run we can execute (replace `smarter_xavier_xxx` with a node name in your cluster):
```bash
kubectl label node smarter_xavier_xxx my-first-ds-app=enabled
```
And we will be able to see after querying the pods in our cluster once more:
```bash
NAME                      READY     STATUS    RESTARTS   AGE       IP           NODE                 NOMINATED NODE   READINESS GATES
my-first-ds-app-<ID>      1/1       Running   0          8s        172.38.0.5   smarter_xavier_xxx   <none>           <none>
```

Labels are a powerful tool in SMARTER, as they can be used to distinguish between diverse characteristics of nodes at the edge, such as geography, compute capability, host operating system etc. For instance, if I have created an app which only runs on nvidia gpus, and fails elsewhere, the DaemonSet I create may specify a node selector like:
```yaml
  nodeSelector:
    my-gpu-app: enabled
    nodetype: jetson
```
This app then will only run on nodes which specify are labeled to run the app **AND** nodes which are labeled as Jetson devices (Xavier, Nano, TX2 etc). I may have a cpu only verision of the same app in which I want to run just on any node that **IS NOT** a Jetson device. In this case we can use a more complex scheduling policy description known as pod affinity:
```yaml
affinity:
  nodeAffinity:
    requiredDuringSchedulingIgnoredDuringExecution:
      nodeSelectorTerms:
      - matchExpressions:
        - key: nodetype
          operator: NotIn
          values:
          - jetson
```
More information on using node affinities is found [here](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#affinity-and-anti-affinity).
The default labels applied to a node can be found [here](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#built-in-node-labels).

As a comparison between pods and DaemonSet objects in k8s, we can perform the experiment of deleting the my-first-ds-app pod above via the cli, similar to how we did in our pod example. If we do so, we will see another pod immediately is spawned to take it's place, because the DaemonSet state still maintains that a pod should be running on that node. If the same application were deployed via just a pod, once deleted, it will not be restarted again.

To remove all pods from all nodes we can delete the DaemonSet by running:
```bash
kubectl delete ds my-first-ds-app
```
or
```bash
kubectl delete -f my-first-ds.yaml
```

To summarize, we prefer the DaemonSet to the pod in SMARTER, as it is better suited to the edge, as it simplifies the deployment process for your application.

## Lesson 2: A simple python app inlined in yaml
In our first lesson, we showed how to deploy an app where the docker image already is publicly hosted via docker hub or other provider. Now we will look at the required steps for deploying an example python application a developer has written, but inlined in the yaml itself.

Create a file named inline-python-ds.yaml with the contents as follows:
```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: inline-python
  namespace: default
  labels:
    k8s-app: inline-python-app
spec:
  selector:
    matchLabels:
      name: inline-python
  template:
    metadata:
      labels:
        name: inline-python
    spec:
      nodeSelector:
        inline-python: enabled
      hostname: inline-python
      containers:
      - name: client
        image: python:3.7.3-alpine3.9
        command: ["python3"]
        args:
        - "-c"
        - |
          import time
          print("Test initiated")
          time.sleep(60)
          print("Test ended")
        imagePullPolicy: IfNotPresent
        resources:
          limits:
            cpu: 200m
            memory: 10Mi
          requests:
            cpu: 200m
            memory: 10Mi
```

This application pattern is useful in order to quickly run a simple python script within a node in your cluster.

To deploy the app, just as above, run:
```bash
kubectl apply -f inline-python-ds.yaml
```

In order to tell the k8s scheduler we want to run a copy of this app on one of our edge nodes, we label it by running:
```bash
kubectl label node smarter_xavier_xxx inline-python=enabled
```

To make sure the app is working as we expect we can check it's logs by running:
```bash
kubectl logs <POD_NAME>
```
which should yield the output:
```bash
Test initiated
Test ended
```
after 60 seconds.

Notice that after every 60 seconds the pod is restarted, as the python exits naturally after 60 seconds, but the DaemonSet still requires a copy of the pod running, forcing it to restart.

To clean up we simply delete the ds by running:
```bash
kubectl delete -f inline-python-ds.yaml
```

## Lesson 3: A container (by hand)
In the case where you have written a sufficiently complex python application, we will now go over how you can package the application using docker and deploy it to the edge. For demonstrative purposes, we will just go ahead and use the above inlined python application, but configured to infinitely loop.

Let's start by creating a new directory `my-app` and inside of that create the python file named `my-app.py` with the contents:
```python
import time
while True:
    print("Test initiated")
    time.sleep(60)
    print("Test ended")
```

Now we must create a Dockerfile which we use to describe how our application is packaged. I won't go over the nitty-gritty of creating Dockerfiles in this guide, but the [Docker documentation online](https://docs.docker.com/engine/reference/builder/) is a great resource for learning more.
```Dockerfile
FROM python:3.7.3-alpine3.9

COPY my-app.py /my-app.py

ENTRYPOINT [ "/usr/local/bin/python3" ]
CMD [ "/my-app.py" ]
```
I will note that for python apps, beware of the default python image tagged `python:<VERSION>`, it is ~1GB, even before you install your python dependencies! For this reason we prefer using alpine linux based images, as it provides a minimal set of userspace apps, and is usually ~100MB. 

Now to build our application for our 3 architectures of interest in one command we use docker buildx. You can learn how to enable buildx on your host machine using the tutorial [here](https://medium.com/@artur.klauser/building-multi-architecture-docker-images-with-buildx-27d80f7e2408)
With buildx installed we need to create a builder and use it, which we do with the following command:
```bash
docker buildx create --name mybuilder
docker buildx use mybuilder
```

Further I recommend setting up a public container registry on dockerhub, or any other public image repository, such that you can push your images up to a location where the edge can pull them down. I am using using dockerhub personally, as it is free for public repos. In the next lesson we will use gitlab's public container registry.

To build and push your application up to your repository run **in the my-app folder**:
```bash
docker login -u <YOUR_REPO_USERNAME>
docker buildx build -t <YOUR_REPO_IMAGE_NAME> --push --platform linux/arm64,linux/arm,linux/amd64 . 
```
The above command will automatically build and push a [Docker manifest](https://docs.docker.com/engine/reference/commandline/manifest/), which is a powerful tool, allowing the same image name string to correspond to multiple docker images with different architectures on the backend. When a node attempts to pull an image string corresponding to a manifest, it will automatically pull down the image of the correct architecture, and fail if it doesn't exist. In the past we often differentiated images based on architecture by prepending the architecture before the image name like `aarch64/Ubuntu:18.04`. This would mean we had to create 3 copies of the same DaemonSet with different image names for each architecture. Manifests made our lives much easier.

Now we can create our yaml for the application in the file `my-app-ds.yaml` with the following contents:
```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: my-app
  namespace: default
  labels:
    k8s-app: my-app
spec:
  selector:
    matchLabels:
      name: my-app
  template:
    metadata:
      labels:
        name: my-app
    spec:
      nodeSelector:
        my-app: enabled
      hostname: my-app
      containers:
      - name: client
        image: <YOUR_REPO_IMAGE_NAME>
        imagePullPolicy: IfNotPresent
        resources:
          limits:
            cpu: 200m
            memory: 10Mi
          requests:
            cpu: 200m
            memory: 10Mi
```
Make sure your replace `<YOUR_REPO_IMAGE_NAME>` with the docker image name you pushed to your remote image registry.

To deploy the app, just as above run:
```bash
kubectl apply -f my-app-ds.yaml
```

In order to tell the k8s scheduler we want to run a copy of this app on one of our edge nodes, we label it by running:
```bash
kubectl label node smarter_xavier_xxx my-app=enabled
```

To make sure the app is working as we expect we can check it's logs by running:
```bash
kubectl logs <POD_NAME>
```
which should yield the output:
```bash
Test initiated
Test ended
```
after each 60 seconds.

To clean up we simply delete the ds by running:
```bash
kubectl delete -f my-app-ds.yaml
```

## Lesson 4: A container (by gitlab)
Like the last lesson, we will use the same app, but instead of building and pushing manually will use gitlab to auto-build the container for us every time we push code to our repository.

### Create gitlab repository
In gitlab create a new blank **public** project named `my-app` with no readme added

Now in your my-app folder create the file `.gitlab-ci.yml` with the contents:
```yaml
include:
  - project: 'ericvh/gitlab-ci-arm-template'
    file: '/.gitlab-ci.yml'

variables:
  CI_BUILDX_ARCHS: "linux/amd64,linux/arm64,linux/arm"
```

This will source a ci script from another SMARTER project repo found [here](https://gitlab.com/ericvh/gitlab-ci-arm-template/-/blob/master/.gitlab-ci.yml) which contains the logic to auto-build docker images on git pushes.

Now we will initialize a git repository in our `my-app` folder and push it to our new gitlab project (replace `<USERNAME>` with your gitlab username>):
```bash
git init
git remote add origin git@gitlab.com:<USERNAME>/my-app.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

Upon the push, you can go to the project in github and click `CI/CD`-->`Pipelines` and see the build job that was spawned on the push.
Because we pushed to master you will see that there was a build job spawned which builds your app using docker build from the Dockerfile in the base of your repo.
When the job is complete if you go to `Packages & Registries`-->`Container Registry`, you will see one image with no tag meaning it corresponds to latest. Every push to master updates the `latest` image for your project. To pin your application version, we can create a tag and push it by running:
```bash
git tag v1.0.0
git push --tags
```

Going back to `CI/CD`-->`Pipelines` we can see a new pipeline started for the tag, which again builds the docker image based on the source code state at the tag version.
Once the job is done you will see in `Packages & Registries`-->`Container Registry` a new image named registry.gitlab.com/`<USERNAME>`/my-app:v1.0.0.

With our new image, go ahead and update the `image` spec in `my-app-ds.yaml` with your new image created just now.

To deploy the app, just as before, run:
```bash
kubectl apply -f my-app-ds.yaml
```

In order to tell the k8s scheduler we want to run a copy of this app on one of our edge nodes, we label it by running:
```bash
kubectl label node smarter_xavier_xxx my-app=enabled
```

To make sure the app is working as we expect we can check it's logs by running:
```bash
kubectl logs <POD_NAME>
```
which should yield the output:
```bash
Test initiated
Test ended
```
after each 60 second interval.

To clean up we simply delete the ds by running:
```bash
kubectl delete -f my-app-ds.yaml
```

## Lesson 5: Deploying container with gitlab
Lesson 5 will expand upon the gitlab ci/cd functionality which implicitly builds docker images for our application, by also automatically deploying updated pods to our edge cluster. 
Gitlab ci/cd is not the easiest to pickup right off the bat, and it takes time to understand how to construct a robust pipeline that fits your needs. In this section I will show you how you can link your SMARTER cluster to our my-app project created above, and spawn a deployment job which deploys or updates the existing DaemonSet for our application.

The first thing we need to do is add a cluster to our my-app repo. That information can be found [here](https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster). With the cluster linked to your account, we can then proceed in updating our gitlab-ci-yaml to leverage the cluster in special jobs.

In your existing my-app repo, open up the `.gitlab-ci-yaml` file and replace the contents with:
```yaml
stages:
  - build
  - deploy

include:
  - project: 'ericvh/gitlab-ci-arm-template'
    file: '/.gitlab-ci.yml'

variables:
  CI_BUILDX_ARCHS: "linux/amd64,linux/arm64,linux/arm"

deploy-production:
  stage: deploy
  image: dtzar/helm-kubectl
  environment:
    name: production
  when: manual
  only:
    - tags
  script:
    - |
      if [[ -z "$CI_COMMIT_TAG" ]]; then
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
      else
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
      fi
    - kubectl describe namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
    - apk add gettext
    - envsubst < my-app-ds.yaml | kubectl apply -n "$KUBE_NAMESPACE" -f -
```

If we compare what existed previously in lesson 4 vs here, we can see we specify that we have not only a `build` stage, but also a `deploy` stage. We add the job `deploy-production`, which specifies in the script that we will deploy the app to the `production` environment, **only** when we push a new tag to the repo. This means that we won't update the DaemonSet in our cluster unless we are ready for an official release of a new version of the application, say tag `v1.1.0`. We also specify that we want to run the deploy to production job manually, meaning a developer with proper credentials will have to ultimately green-light the deployment and manually trigger the job.

When a job specifies an environment, the job automatically picks up the linked kubernetes cluster credentials, and the script will automatically set the environment variables listed [here](https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster). The most important variable in this section is `$KUBECONFIG`, which tells kubectl how to connect to our cluster with the proper credentials. Gitlab automatically creates the proper kubeconfig for us, meaning we can run normal kubectl commands in your script that talk to our cluster. 

The final piece to note here is that we use envsubst to replace all the environment variable placeholders in our application DaemonSet file with their values. Open `my-app-ds.yaml` and replace the contents with:
```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: my-app
  annotations:
    app.gitlab.com/app: "$CI_PROJECT_PATH_SLUG"
    app.gitlab.com/env: "$CI_ENVIRONMENT_SLUG"
  labels:
    k8s-app: my-app
spec:
  selector:
    matchLabels:
      name: my-app
  template:
    metadata:
      labels:
        name: my-app
    spec:
      nodeSelector:
        my-app: enabled
      hostname: my-app
      containers:
      - name: client
        image: $CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG
        imagePullPolicy: IfNotPresent
        resources:
          limits:
            cpu: 200m
            memory: 10Mi
          requests:
            cpu: 200m
            memory: 10Mi
```
We replace the image name in this container spec with the values of the pipeline run. When we create and push a git tag, the image here will be populated with your base repo image name and tagged with the appropriate tag (ie `v1.1.0`).

With the files updated in your repo commit your changes, and create a tag by running:
```bash
git commit -am "Add deploy policy"
git tag v1.1.0
git push --tags
```

When the push goes through, if you navigate to the gitlab repo and go to `CI/CD`-->`Jobs`, you will see a pipeline with the build and deploy stages activated. When build finishes, you will see that the deploy stage is awaiting you to click on it to manually trigger the pipeline. Trigger the pipeline, and now back on our command line, you will be able to see the new pods spun up in the gitlab created namespace after a short time by running:
```bash
kubectl get pods --all-namespaces
```
which should show something like:
```bash
NAMESPACE                    NAME             READY     STATUS             RESTARTS   AGE
my-app-18943481-production   my-app-h42mr     1/1       Ready              0          50s
my-app-18943481-production   my-app-c9gbf     1/1       Ready              0          50s
```
Note this assumes we still have our nodes labeled with `my-app=enabled`.

Of course this is the most basic example for deploying your app to SMARTER via gitlab that you could come up with. Ideally you would add testing stages, canary deployments, and many other bells and whistles to ensure your application is fool-proof before deploying it to your production edge. I will go over how this can be done in a future tutorial. For a fully  built out example of how an app is deployed to a cloud k8s cluster you can check out [this](https://gitlab.com/williamchia/cncf-demo-2/-/blob/master/.gitlab-ci.yml) repository.

### Using ImagePullSecrets for your private images
The tutorial [here](https://www.informaticsmatters.com/blog/2020/07/26/deploying-images-from-a-gitlab-registry.html) shows you exactly how to create a secret for gitlab repositories in your cluster which can be referenced from other cluster resources like DaemonSets or Deployments.
This allows you to keep your gitlab projects private, and still reference images from the container registry in your SMARTER deployments.

To create the secret first you must create a deploy token using gitlab:
1. Go to Settings->Repository and expand the deploy tokens menu in the project containing your built images
2. Add a deploy token by setting an name, expiration date, and username, and checking read_registry scope then press Deploy

Now go ahead and from the command line create a pull secret by replacing the templates for username (username for token creation) and token (generated in the above step) in the following commands:
```bash
gitlab_user=<Username>
gitlab_token=<Token>
gitlab_pull_secret=$(echo -n "{\"auths\":{\"registry.gitlab.com\":{\"auth\":\"`echo -n "$gitlab_user:$gitlab_token"|base64`\"}}}"|base64)
```

Now to fetch your created base64 secret value, run
```bash
echo gitlab_pull_secret
```

Create a secret yaml file with the following contents named `image-pull-secret.yaml`:
```yaml
kind: Secret
apiVersion: v1
metadata:
  name: gitlab-pull-secret
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: <the pull secret Base64 string>
```

Now apply the file to your cluster by running:
```bash
kubectl apply -f image-pull-secret.yaml
```

Now for any DaemonSet in our cluster which requires an image string, we can also provide an imagePullSecret to the yaml. Taking the above example we can simply add:
```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: my-app
  annotations:
    app.gitlab.com/app: "$CI_PROJECT_PATH_SLUG"
    app.gitlab.com/env: "$CI_ENVIRONMENT_SLUG"
  labels:
    k8s-app: my-app
spec:
  selector:
    matchLabels:
      name: my-app
  template:
    metadata:
      labels:
        name: my-app
    spec:
      nodeSelector:
        my-app: enabled
      hostname: my-app
      containers:
      - name: client
        image: $CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG
        imagePullPolicy: IfNotPresent
        imagePullSecrets:
        - name: gitlab-pull-secret
        resources:
          limits:
            cpu: 200m
            memory: 10Mi
          requests:
            cpu: 200m
            memory: 10Mi
```

# Platform
## Lesson 6: Reading data from a device (using device manager)
As part of the SMARTER project, we have developed a device manager used to enforce restrictions on what pods can use devices attached to the node, without granting the pods elevated privilege. You can read about the project [here](https://community.arm.com/developer/research/b/articles/posts/a-smarter-device-manager-for-kubernetes-on-the-edge)

To use the smarter device manager you must create the yaml description for it's DaemonSet. Create the file `smarter-device-manager-ds.yaml` and populate it with the file contents:
```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: smarter-device-manager
  namespace: default
  labels:
    name: smarter-device-manager
    role: agent
spec:
  selector:
    matchLabels:
      name: smarter-device-manager
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels: 
        name: smarter-device-manager
      annotations:
        node.kubernetes.io/bootstrap-checkpoint: "true"
    spec: 
      nodeSelector:
        smarter-device-manager : enabled
      priorityClassName: "system-node-critical"
      hostname: smarter-device-management
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
      containers:
      - name: smarter-device-manager
        image: registry.gitlab.com/arm-research/smarter/smarter-device-manager/smarter-device-manager:20191204204613
        imagePullPolicy: IfNotPresent
        securityContext:
          allowPrivilegeEscalation: false
          capabilities:
            drop: ["ALL"]
        resources:
          limits:
            cpu: 100m
            memory: 15Mi
          requests:
            cpu: 10m
            memory: 15Mi
        volumeMounts:
          - name: device-plugin
            mountPath: /var/lib/kubelet/device-plugins
          - name: dev-dir
            mountPath: /dev
      volumes:
        - name: device-plugin
          hostPath:
            path: /var/lib/rancher/k3s/agent/kubelet/device-plugins
        - name: dev-dir
          hostPath:
            path: /dev
      terminationGracePeriodSeconds: 30
```

As a demonstrative example of how we can use this to manage our devices, plugin a usb microphone and camera into one of your nodes. We generally use a ps3 eye cam for these purposes, but most usb mics/cameras should work fine.

Now we can apply the file and label our test node to deploy the app:
```bash
kubectl apply -f smarter-device-manager-ds.yaml
kubectl label node smarter_xavier_xxx smarter-device-manager=enabled
```

Now if you describe you target node with the mic plugged in by running:
```bash
kubectl describe node smarter_xavier_xxx
```
assuming all went well you should see something like:
```bash
Allocatable:
 cpu:                        8
 ephemeral-storage:          27985794436
 hugepages-2Mi:              0
 memory:                     16194300Ki
 pods:                       110
 smarter-devices/gpiochip0:  10
 smarter-devices/gpiochip1:  10
 smarter-devices/gpiochip2:  10
 smarter-devices/i2c-0:      10
 smarter-devices/i2c-1:      10
 smarter-devices/i2c-2:      10
 smarter-devices/i2c-3:      10
 smarter-devices/i2c-4:      10
 smarter-devices/i2c-5:      10
 smarter-devices/i2c-6:      10
 smarter-devices/i2c-7:      10
 smarter-devices/i2c-8:      10
 smarter-devices/rtc0:       10
 smarter-devices/snd:        10
 smarter-devices/video0:     10
```

And without the microphone/camera plugged in we should see that `smarter-devices/video0` is 0 (Generally /dev/snd will exist to manage any sound outputs built onto a SoCs dev-board).

To describe that an application wishes to use a device in a yaml spec, we look at the following example:
```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: pulseaudio
  labels:
    k8s-app: pulseaudio
spec:
  selector:
    matchLabels:
      name: pulseaudio
  template:
    metadata:
      labels:
        name: pulseaudio
    spec:
      nodeSelector:
        pulse: "enabled"
      hostname: pulse
      containers:
      - name: pulseaudio
        imagePullPolicy: IfNotPresent
        image: registry.gitlab.com/arm-research/smarter/edge-workloads/pulseaudio:v1.0.0
        resources:
          limits:
            cpu: 350m
            memory: 100Mi
            smarter-devices/snd: 1
          requests:
            cpu: 250m
            memory: 100Mi
            smarter-devices/snd: 1
      terminationGracePeriodSeconds: 15
```

We can see that this DaemonSet requests to use the `smarter-devices/snd` device (aka `/dev/snd`). When deployed this application spins up a pulseaudio server that allows other pulseaudio clients to establish tcp connections to such that multiple clients applications can use the same microphone concurrently without having to each request access to `/dev/snd`.

At this time the SMARTER device manager does not manage accelerators like gpus or tpus, however this is a planned feature. 

## Lesson 7: Use MQTT to publish data to fluent bit
[Fluent Bit](https://docs.fluentbit.io/manual/) is a lightweight message processing service which serves as a great tool for brokering IoT data. One great feature of fluent bit is the ability to bring up an MQTT broker input for clients to publish data to for further processing. From fluent bit, you can manipulate the message data format, run stream processing, and output to one of many supported output plugins such as elasticsearch or influxdb for long term storage. 

In this example we will show a simple example of how fluent bit can be brought up at the edge with an MQTT input, and how a client may publish data. 

For the deployment of fluent bit this repo [directory](https://gitlab.com/arm-research/smarter/example/-/tree/v0.5.2/yaml/fluent-bit) provides our exact configuration which provides log scraping for each of the pods in our system, and provides an MQTT source which clients publish messages to. The example of how this is deployed in the context of our SMARTER example is found [here](https://gitlab.com/arm-research/smarter/example/-/blob/v0.5.2/README.md#run-the-demo).

As a simple python snippet of what it looks like to publish a message to the fluent bit MQTT server from a pod, we do the following:
```python
import json
import os
import paho.mqtt.publish as publish

MQTT_BROKER_HOST = os.getenv('MQTT_BROKER_HOST', 'fluent-bit')
TOPIC = os.getenv('TOPIC', '/demo/messages')

msg = { 'key1': 'value1', 'key2': 'value2' }

try:
    publish.single(TOPIC, json.dumps(msg), hostname=MQTT_BROKER_HOST)
    print('Message published to mqtt: {}'.format(msg))
except Exception as e:
    print('Sound classified successfully but mqtt publish failed with error: {}'.format(e))
```

Notice that in this example we specify any hostnames used for remote services as environment variables, such that we can easily configure them using the yaml description, without having to rebuild the app each time we make a change.

## Lesson 8: Modifying how fluent bit processes and directs data
In the previous lesson we showed how fluent bit can be used to ingest MQTT data along with simple python snippet. Once the data is in fluent bit, we can massage and manipulate it as much as we'd like, along with specify where we would like to push the results out to. Given that the edge is a dynamic environment in which data routing may need to change on the fly, this lesson will go over how the fluent bit configuration can be updated on the fly. 

We will reference the ConfigMap from the repo directory referenced in lesson 7. If we look at the file: `fluent-bit-configmap.yaml`, we can see the following section:
```yaml
output-fluentd.conf: |
    [OUTPUT]
        Name            forward
        Match           *_count
        Host            ${FLUENTD_HOST}
        Port            ${FLUENTD_PORT}
        Shared_Key      ${FLUENTD_PASSWORD}
        Self_Hostname   ${MY_NODE_NAME}
        tls             on
        tls.verify      off
    [OUTPUT]
        Name            forward
        Match           *_class
        Host            ${FLUENTD_HOST}
        Port            ${FLUENTD_PORT}
        Shared_Key      ${FLUENTD_PASSWORD}
        Self_Hostname   ${MY_NODE_NAME}
        tls             on
        tls.verify      off
    [OUTPUT]
        Name            forward
        Match           netdata
        Host            ${FLUENTD_HOST}
        Port            ${FLUENTD_PORT}
        Shared_Key      ${FLUENTD_PASSWORD}
        Self_Hostname   ${MY_NODE_NAME}
        tls             on
        tls.verify      off
```

In this section we are specifying that we want to output data using the `forward` output, which allows us to send all messages which are tagged with the `Match` parameter to either fluentd or another fluent bit instance for further processing. Notice we use a `*` in our `Match` field as a wild-card specifying that any message topic that ends with `_count`, will be forwarded to fluentd. The `Host` field is set to read from an environment variable, such that we can modify the environment variable in the DaemonSet container spec without having to make the change 3 times in this ConfigMap.

In the case that we have fluent bit running on each node with the current configuration, we can update the config map by editing it in place or creating a copy of it and making changes there.

Say we decided to just print all messages which match the tag `*_count` to stdout rather than forward. We could simply edit the above to:
```yaml
output-fluentd.conf: |
    [OUTPUT]
        Name            stdout
        Match           *_count
    [OUTPUT]
        Name            forward
        Match           *_class
        Host            ${FLUENTD_HOST}
        Port            ${FLUENTD_PORT}
        Shared_Key      ${FLUENTD_PASSWORD}
        Self_Hostname   ${MY_NODE_NAME}
        tls             on
        tls.verify      off
    [OUTPUT]
        Name            forward
        Match           netdata
        Host            ${FLUENTD_HOST}
        Port            ${FLUENTD_PORT}
        Shared_Key      ${FLUENTD_PASSWORD}
        Self_Hostname   ${MY_NODE_NAME}
        tls             on
        tls.verify      off
```

To update the app, unfortunately you cannot simply just reapply this ConfigMap for the changes to be picked up (although Helm lets you do this via `helm update`). Instead we must reapply the ConfigMap, then delete and reapply the DaemonSet which references the ConfigMap. In the case of our fluent bit example this would look something like:
```bash
kubectl apply -f fluent-bit-configmap.yaml
kubectl delete -f fluent-bit-ds.yaml
kubectl apply -f fluent-bit-ds.yaml
```

At this point you would be able to see all pods corresponding to the old DaemonSet terminating, and new pods coming up in their place which correspond to the new DaemonSet.

## Lesson 9: Using grafana to see your data
In our final lesson, we will go over how grafana can be used to visualize the data collected at the edge. With the current iteration of the SMARTER infrastructure, we made the decision to create a separately managed k3s cluster which lives entirely in the cloud, such that we can use many of cloud native tools to deploy complex applications like prometheus, influxdb, and grafana, to list some examples. 

I will assume that you have setup your cloud cluster using the instructions in the following [README](https://gitlab.com/arm-research/smarter/example/-/blob/master/cloud-data-node.md).

By following the above instructions, you will have a publicly available grafana instance in the cloud. Navigate to grafana at https://grafana-${SMARTER_DATA_DOMAIN} in your browser. In grafana, the general pattern we must follow to create a visualization is to first create a data source. To do so click on the gear on the left menu and select Data Sources from the drop down menu. As part of the cloud install process we created an influxdb instance, and registered a database named `waggledemo`. To use this data source, select `Add data source` and select influxdb from the list. As the title of the data source (as grafana recognizes it) select InfluxDB-Waggle. In the URL put http://influxdb:8086, and for the database enter the name waggledemo. Then press save and test, you should get a green popup on the bottom indicating success. Exit back to the home page.

Now we must create a visualization which sources from the influxdb database we just created. On the left menu select the plus and from the dropdown select import, then select Upload .json file and choose the file dashboards/smarter-grafana-demo-dashboard.json in the `example` repo and press import.

Looking at the grafana dashboard, we can edit the queries by selecting the dropdown next to a chart name and selecting edit. In the example dashboard you've loaded, you can see that we are querying data from influxdb, and this can be modified to your liking. 

Just like gitlab ci/cd, grafana is it's own complex world, which you could spend months learning all the ins and outs. For influxdb and grafana more comprehensive documentation can be found [here](https://grafana.com/docs/grafana/latest/features/datasources/influxdb/).

## Lesson 10: Use Gstreamer to provide a video to clients via IP
In order to supply the same camera feed to multiple clients without granting access to the camera we use Gstreamer with an RTSP server frontend. Clients then simply supply a rtsp url and are able to access camera data.

### Functionality
- https://gstreamer.freedesktop.org/
- Configures gstreamer video server that manages all video devices available on host
- Actors are not granted access to dev/video*, but connect to Gstreamer server and receive/stream video over network

For use in kubernetes we have opted to split out a frontend and backend pipeline. The frontend pipeline is responsible for taking a raw camera or video file stream, and performing non-computationally expensive operations (resize, format convert, or framerate convert). The frontend pipeline tees the processed image feed into n shared memory sinks which the backend can then pickup.

For the backend pipeline, we create a rtsp server which launches a backend pipeline per requested rtsp feed. For instance if a client were to request h264 encoded frames at 1 fps, we would launch a new gstreamer process which reads from the shared mem channel for 1 fps raw frames, then h264 encodes them, and sends them to the client. The backend rtsp server is designed to multicast data to m clients if they request the same feed, as to not duplicate encoding work.

From the client perspective users simply request frames from the url rtsp://\<hostname\>:8554/video.<raw/h264>.<framerate>. In the k8s yaml, the frontend pipeline can be constructed to read from any arbitrary source, be it an rtsp source, physical camera, or file. The backend pipelines spawned by the rtsp server are defined in the c file.

In the context of an opencv application, users can access the camera data like the following:
```python
def getframe():
  cam = cv2.VideoCapture('rtsp://gstreamer:8554/video.h264.30')
  if (cam.isOpened() == False):
      print('Error opening video stream ', 'rtsp://gstreamer:8554/video.h264.30')
      exit(1)
  while True:
      ret, frame = cam.read()
      if not ret:
          print('No camera found')
          exit(0)
      yield frame
  cam.release()
```

### Custom pipelines
The current SMARTER example assumes we are using a USB camera which reads frames at 30fps at resolution 640x480. If this is not the case you must alter the configuration for the gstreamer server. This can be done in the pod yaml as specified in the gstreamer daemonset. Looking at the command and args passed to the pod:
```bash
command: ["/bin/bash"]
args: ["-c", "gst-launch-1.0 v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=640,height=480,framerate=30/1 ! videoconvert ! video/x-raw,format=I420 ! tee name=30raw ! queue ! shmsink socket-path=/tmp/gstreamer/raw-30 shm-size=100000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=1/1 ! shmsink socket-path=/tmp/gstreamer/raw-1 shm-size=20000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=5/1 ! shmsink socket-path=/tmp/gstreamer/raw-5 shm-size=20000000 wait-for-connection=false sync=false 30raw. ! queue ! videorate ! video/x-raw,framerate=10/1 ! shmsink socket-path=/tmp/gstreamer/raw-10 shm-size=20000000 wait-for-connection=false sync=false"]
```
We see that we are using a v4l2src reading from /dev/video0, with format `video/x-raw,format=YUY2,width=640,height=480,framerate=30/1`. We then convert to format `video/x-raw,format=I420` such that the stream can be h264 encoded if necessary. If you provide your own camera, you may have to change device where the camera is located as well as the format that the camera reads out. Everything else should remain the same. The only caveat is if your default resolution increases, you need to increase the size of the shmsink by the order of magnitude that the resolution increased.

## Lesson 11: Using Triton for inference
Nvidia Triton is an inference server which allows clients to send gRPC or HTTP requests with tensor data to perform inference on. It separates the hassle of working with ML runtimes from the clients, and allows users to just bring their own serialized models along with their metadata and it takes care of the rest. In the SMARTER example, we use Triton to serve our object classification model as well as ambient sound classifier. Users of triton can specify whether they want an instance of the model to run on cpu or gpu. SMARTER has extended Triton with the ability to run ArmNN models on platforms which don't support CUDA, allowing us to leverage the acceleration benefits of using Arm Neon instructions as well as Arm Mali GPUs. 

The following directions will show you how to add your own model to your Triton image and setup a client in python to perform inference using the client API.

### Add Model to Triton
For tensorflow let's assume we have the a saved model in pb format, named `ssd_mobilenetv1_coco_optimized.pb`. We first need to create a directory that triton will use to source it's model data from. We create the directory `triton-models` and within it we add the subdirectory for our object classification model `ssd_mobilenetv1_coco`:
```bash
mkdir -p triton-models/ssd_mobilenetv1_coco
cd triton-models/ssd_mobilenetv1_coco
```
Now within this new directory we must create a folder for the first version of the model (triton allows you to have multiple versions of the same model) and we place our saved model within this directory under the name `model.graphdef`:
```bash
mkdir 1
cp ~/ssd_mobilenetv1_coco_optimized.pb 1/model.graphdef
```
With our model now in it's proper location we must add a file named `config.pbtxt` to the model directory alongside the folder named `1`. In this file we put information about the model format, input and output tensor data, among other things. For this model we create the following `config.pbtxt`:
```bash
name: "ssd_mobilenet_coco"
platform: "tensorflow_graphdef"
max_batch_size: 1
input [
  {
    name: "image_tensor"
    data_type: TYPE_UINT8
    format: FORMAT_NHWC
    dims: [ 300, 300, 3 ]
  }
]
output [
  {
    name: "detection_boxes"
    data_type: TYPE_FP32
    dims: [ 100, 4 ]
  },
  {
    name: "detection_classes"
    data_type: TYPE_FP32
    dims: [ 100 ]
  },
  {
    name: "detection_scores"
    data_type: TYPE_FP32
    dims: [ 100 ]
  },
  {
    name: "num_detections"
    data_type: TYPE_FP32
    dims: [ 1 ]
    reshape: { shape: [ ] }
  }
]
```
In this file you can see we specify the max batch size as 1, meaning the rest of the tensor dimensions assume a variable first dimension and we only have to worry about the dimensions that follow. We specify the names of the tensors in the model for inputs and outputs along with their shapes. For more info on configuration options refer to the [Triton docs](https://docs.nvidia.com/deeplearning/triton-inference-server/user-guide/docs/model_configuration.html).

### Setup Triton Client in Python
With Triton running referencing the model above, we can setup a gRPC triton client in python like the following:
```python
import argparse
import os
import numpy as np
import cv2
import tritongrpcclient
from tritonclientutils import InferenceServerException

parser.add_argument('-m', '--model-name', type=str, required=False,
                    default=os.getenv('MODEL_NAME', 'ssd_mobilenet_coco'), help='Name of model')
parser.add_argument('-x', '--model-version', type=str, required=False, default="",
                    help='Version of model. Default is to use latest version.')
parser.add_argument('-v', '--verbose', action="store_true",
                    required=False, default=False, help='Enable verbose output')
parser.add_argument('-u', '--url', type=str, required=False, default=os.getenv('TRITON_URL', 'localhost:8000'),
                    help='Inference server URL. Default is localhost:8000.')


def read_classes(path):
  classes = {}
  with open(path) as file:
      for line in file:
          fields = line.split()
          classes[int(fields[0])] = fields[1]
  return classes


def getframe():
  cam = cv2.VideoCapture('rtsp://gstreamer:8554/video.h264.30')
  if (cam.isOpened() == False):
    print('Error opening video stream ', 'rtsp://gstreamer:8554/video.h264.30')
    exit(1)
  while True:
    ret, frame = cam.read()
    if not ret:
      print('No camera found')
      exit(0)
    yield frame
  cam.release()


def validate_model_grpc(model_metadata, model_config):
  """
  Check the configuration of a model to make sure it meets the
  requirements for ssd_mobilenet_v1 (as expected by
  this client)
  """
  if len(model_metadata.inputs) != 1:
    raise Exception("expecting 1 input, got {}".format(
      len(model_metadata.inputs)))
  if len(model_metadata.outputs) != 4:
    raise Exception("expecting 4 outputs, got {}".format(
      len(model_metadata.outputs)))

  if len(model_config.input) != 1:
    raise Exception(
      "expecting 1 input in model configuration, got {}".format(
      len(model_config.input)))

  for output_metadata in model_metadata.outputs:
    if output_metadata.datatype != "FP32":
    raise Exception("expecting output datatype to be FP32, model '" +
                      model_metadata.name + "' output type is " +
                      output_metadata.datatype)

  return model_metadata.inputs[0].name, [output.name for output in model_metadata.outputs]


def infer_image(
      clientclass, client, model_name, model_version, input_name, output_names, imgorig, classes):
  img_rows = imgorig.shape[0]
  img_cols = imgorig.shape[1]
  resized = cv2.resize(imgorig, (300, 300))
  converted = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)

  # create input
  request_input = clientclass.InferInput(
  input_name, [1, 300, 300, 3], "UINT8")
  request_input.set_data_from_numpy(
      np.expand_dims(converted, axis=0))

  # create output
  detection_boxes_request = clientclass.InferRequestedOutput(
      output_names[0])
  detection_classes_request = clientclass.InferRequestedOutput(
      output_names[1])
  detection_probs_request = clientclass.InferRequestedOutput(
      output_names[2])
  num_detections_request = clientclass.InferRequestedOutput(
      output_names[3])

  results = client.infer(model_name, (request_input,), model_version=model_version, outputs=(
      detection_boxes_request, detection_classes_request, detection_probs_request, num_detections_request))

  detection_boxes = results.as_numpy(output_names[0])
  detection_classes = results.as_numpy(output_names[1])
  detection_probs = results.as_numpy(output_names[2])
  num_detections = results.as_numpy(output_names[3])

  # Iterate through detection list and print detection numbers
  detected_objects = {}
  for i in range(int(num_detections[0])):
    if detection_probs[0][i] > 0.5:
      detection_class_idx = detection_classes[0][i]

      # For some reason after converting ssd_mobilenet_v1 to tflite and then armnn the detection classes
      # shift by one
      detection_class = classes[detection_class_idx]
      if detection_class not in detected_objects:
        detected_objects[detection_class] = {}
      detection_index = len(detected_objects[detection_class].keys())
      bbox = detection_boxes[0][i]
      left = int(bbox[1] * img_cols)
      top = int(bbox[0] * img_rows)
      right = int(bbox[3] * img_cols)
      bottom = int(bbox[2] * img_rows)

      detected_objects[detection_class][detection_index] = (
          left, top, right, bottom)

  return detected_objects


if __name__ == '__main__':
  # Create gRPC client for communicating with the server
  triton_client = tritongrpcclient.InferenceServerClient(
    url=args.url, verbose=args.verbose)

  # Make sure the model matches our requirements, and get some
  # properties of the model that we need for preprocessing
  try:
    model_metadata = triton_client.get_model_metadata(
      model_name=args.model_name, model_version=args.model_version)
  except InferenceServerException as e:
    print("failed to retrieve the metadata: " + str(e))
    sys.exit(1)

  try:
    model_config = triton_client.get_model_config(
      model_name=args.model_name, model_version=args.model_version)
  except InferenceServerException as e:
    print("failed to retrieve the config: " + str(e))
    sys.exit(1)

  input_name, output_names = validate_model_grpc(
    model_metadata, model_config.config)
  tritonclass = tritongrpcclient

  # Read output classes of model from file
  classes = read_classes('ssd_mobilenet_coco.classes')

  # Generate client request in loop using data read from opencv cam
  for img in getframe():
    detected_objects = infer_image(tritonclass, triton_client, args.model_name, args.model_version,
                                    input_name, output_names, img, classes)

    time.sleep(1)
```


